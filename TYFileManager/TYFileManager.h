//
//  TYFileManager.h
//  TYFileManager
//
//  Created by 夏伟 on 2016/10/20.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <FCFileManager/FCFileManager.h>

@interface TYFileManager : FCFileManager

// 创建Cache下的文件夹
+ (BOOL)createDirInCache:(NSString *)dirName;

@end

//
//  TYFileManager.m
//  TYFileManager
//
//  Created by 夏伟 on 2016/10/20.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYFileManager.h"

@implementation TYFileManager

+ (BOOL)createDirInCache:(NSString *)dirName {
    NSString *dirPath = [self pathForCachesDirectoryWithPath:dirName];
    return [self createDirectoriesForPath:dirPath];
}

@end

Pod::Spec.new do |s|
  s.name = 'TYFileManager'
  s.version = '0.0.1'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'iOS File Manager on top of NSFileManager for simplifying files management. (基于FCFileManager)'
  s.homepage = 'https://gitlab.com/xwgit2971/TYFileManager'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYFileManager.git', :tag => s.version }
  s.source_files = 'TYFileManager/*.{h,m}'
  s.framework = 'Foundation'
  s.requires_arc = true
  s.dependency  'FCFileManager', '~> 1.0.18'
end

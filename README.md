# TYFileManager
iOS File Manager on top of NSFileManager for simplifying files management. (基于FCFileManager)

## How To Use

```objective-c
#import <TYFileManager/TYFileManager.h>
```

更详细的用法请参考 https://github.com/fabiocaccamo/FCFileManager
